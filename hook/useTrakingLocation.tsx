import { useState, useContext } from "react";
import { storeContext } from "../store/storeContext";

interface ITrackLocation {
  handleTrackLocation: () => void;
  locationErrorMsg: string;
  isFindingLocation: boolean;
}

const useTrakingLocation = (): ITrackLocation => {
  const [locationErrorMsg, setLocationErrorMsg] = useState("");
  const [isFindingLocation, setIsFindingLocation] = useState(false);
  const { dispatch } = useContext(storeContext);

  const success = (position: any): void => {
    const latitude = position.coords.latitude;

    const longitude = position.coords.longitude;

    dispatch({
      type: process.env.NEXT_PUBLIC_SET_LAT_LONG || "",
      payload: { Lat: `${latitude}`, lng: `${longitude}` },
    });

    setLocationErrorMsg("");
    setIsFindingLocation(false);
  };

  const error = (): void => {
    setLocationErrorMsg("متاسفانه موقعیت مکانی شما را یافت نکردیم.");
    setIsFindingLocation(false);
  };

  const handleTrackLocation = (): void => {
    setIsFindingLocation(true);
    if (!navigator.geolocation) {
      setLocationErrorMsg("مرورگر شما قادر به دریافت موقعیت مکانی نمیباشد.");
      setIsFindingLocation(false);
    } else {
      navigator.geolocation.getCurrentPosition(success, error);
    }
  };

  return { handleTrackLocation, locationErrorMsg, isFindingLocation };
};

export default useTrakingLocation;
