import Link from "next/link";
import styles from "./InfoBox.module.css";

type Props = {
  title: string;
  value: string;
  isLink?: boolean;
};

const InfoBox = (props: Props): JSX.Element => {
  const { title, value, isLink } = props;
  return (
    <div className={styles.wrapperInfoBox}>
      <span className={styles.title}>{title}</span>
      {isLink ? (
        <Link
          href={`${process.env.NEXT_PUBLIC_BASE_URL}${value}`}
          className={styles.valueLink}
        >
          {value}
        </Link>
      ) : (
        <p className={styles.value}>{value}</p>
      )}
    </div>
  );
};

export default InfoBox;
