import axios from "axios";
import { createApi } from "unsplash-js";

// const unsplash = createApi({
//   accessKey: process.env.NEXT_PUBLIC_COFFEE_IMAGES_API_KEY!,
// });
const getcoffeeUrl = (query: string, lat: string, lng: string): string => {
  return `https://api.neshan.org/v1/search?term=${query}&lat=${lat}&lng=${lng}`;
};

// export const getListCoffeeStoreImages = async (
//   query: string,
//   perPage: number
// ): Promise<string[] | undefined> => {
//   const photos = await unsplash.search.getPhotos({
//     query,
//     perPage,
//     orientation: "portrait",
//   });

//   const results = photos.response?.results;
//   return results?.map((item): string => item.urls.regular);
// };

export const getCoffeeStoreApi = async (
  Lat = "35.79575168925595",
  lng = "50.89745576402294"
): Promise<any> => {
  // const photos = await getListCoffeeStoreImages("coffee shop", 30);
  const options = {
    method: "GET",
    headers: {
      "Api-Key": process.env.NEXT_PUBLIC_COFFEE_STORE_API_KEY,
    },
  };

  const response = await axios(getcoffeeUrl("coffee", Lat, lng), options);
  const results = response.data.items;

  return results.slice(0, 6).map((result: any, inx: number): any => {
    // return {
    //   id: result.fsq_id || null,
    //   name: result.name || null,
    //   shortAddress: result.location.address || null,
    //   AddressInDetail: result.location.formatted_address || null,
    //    imgUrl: photos && photos[inx],
    // };
    return {
      id: inx || null,
      name: result.title || null,
      shortAddress: result.region || null,
      AddressInDetail: result.address || null,
    };
  });
};
