import Image, { StaticImageData } from "next/image";
import styles from "./HeroImage.module.css";

type Props = {
  url: StaticImageData;
  alt: string;
};

const HeroImage = (props: Props) => {
  const { url, alt } = props;
  return (
    <div className={styles.wrapperHeroImage}>
      <Image src={url} alt={alt} />
    </div>
  );
};

export default HeroImage;
