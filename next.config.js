/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  images: {
    domains: [
      "s6.uupload.ir",
      "uupload.ir",
      "coffee.alexflipnote.dev",
      "images.unsplash.com",
    ],
  },
};

module.exports = nextConfig;
