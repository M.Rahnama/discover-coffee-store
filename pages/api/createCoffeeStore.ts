import { NextApiRequest, NextApiResponse } from "next";
import { getSpecificRecords, table } from "../../lib/airTable";

const createCoffeeStore = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  try {
    if (req.method === "POST") {
      const { id, name, AddressInDetail, imgUrl } = req.body;
      if (id) {
        const coffeeStoreFindRecord = await table
          .select({
            filterByFormula: `id="${id}"`,
          })
          .firstPage();

        if (coffeeStoreFindRecord.length) {
          const recordsFound = getSpecificRecords(coffeeStoreFindRecord);
          res.json(recordsFound);
        } else {
          if (name) {
            const createRecord = await table.create([
              {
                fields: {
                  id,
                  name,
                  AddressInDetail,
                  imgUrl,
                },
              },
            ]);
            const newRecords = getSpecificRecords(createRecord);
            res.json(newRecords);
          } else {
            res.status(400);
            res.json({ message: "نام کافه ثبت نشده" });
          }
        }
      } else {
        res.status(400);
        res.json({ message: "شناسه کافه ثبت نشده" });
      }
    }
  } catch (error) {
    res.status(500);
    res.json({ message: "کافه ای یافت نشد!", error });
  }
};
export default createCoffeeStore;
