import React from "react";
import useTrakingLocation from "../../hook/useTrakingLocation";
import styles from "./Banner.module.css";

type Props = {
  title: string;
  description: string;
  searchButtonTilte: string;
  handleLocation: () => void;
};

const Banner = (props: Props) => {
  const { locationErrorMsg } = useTrakingLocation();

  const { title, description, searchButtonTilte, handleLocation } = props;
  return (
    <div className={styles.wrapperBanner}>
      <h1 className={styles.title}>{title}</h1>
      <p className={styles.description}>{description}</p>
      <button className={styles.searchButtonTilte} onClick={handleLocation}>
        {searchButtonTilte}
      </button>
      {locationErrorMsg && (
        <p className={styles.notFoundLocationText}> {locationErrorMsg}</p>
      )}
    </div>
  );
};

export default Banner;
