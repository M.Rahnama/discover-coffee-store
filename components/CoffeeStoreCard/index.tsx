import Image from "next/image";
import styles from "./CoffeeStoreCard.module.css";
import CoffeeImage from "./components/CoffeeImage";
import InfoBox from "./components/InfoBox";
import { useState } from "react";

type Props = {
  data: {
    name: string;
    AddressInDetail: string;
    // imgUrl: string;
    link: string;
  };
};

const CoffeeStoreCard = (props: Props): JSX.Element => {
  const { data } = props;
  const [vote, setVote] = useState(1);

  const handleVoting = (): void => {
    const count = vote + 1;
    setVote(count);
  };

  return (
    <div className={styles.wrapperCardCoffeeStore}>
      {/* <CoffeeImage imageUrl={data.imgUrl} alt={data.name} /> */}
      <div className={styles.wrapperCoffeeInfo}>
        <InfoBox title={"نام کافه "} value={data.name} />
        <InfoBox title={"آدرس "} value={data.AddressInDetail} />
        {/* <button
          type="button"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          <svg
            aria-hidden="true"
            className="w-4 h-4"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
              clip-rule="evenodd"
            ></path>
          </svg>
        </button> */}
      </div>
    </div>
  );
};

export default CoffeeStoreCard;
