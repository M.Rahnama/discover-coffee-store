import { createContext, Dispatch, useReducer } from "react";

interface IstoreContext {
  state: any;
  dispatch: Dispatch<{ payload: any; type: string }>;
}

export const storeContext = createContext<IstoreContext>({
  state: null,
  dispatch: (dispatch: { payload: any; type: string }): void => {},
});

const Status = {
  SET_LAT_LONG: "SET_LAT_LONG",
  SET_COFFEE_STORES: "SET_COFFEE_STORES",
} as const;

type StatusType = typeof Status[keyof typeof Status];

const storeReducer = (
  state: any,
  action: {
    payload: any;
    type: string;
  }
): any => {
  const StatusMap = new Map<StatusType, {}>([
    [
      Status.SET_LAT_LONG,
      {
        ...state,
        Lat: action.payload.Lat,
        lng: action.payload.lng,
      },
    ],
    [
      Status.SET_COFFEE_STORES,
      {
        ...state,
        coffeeStores: action.payload.coffeeStores,
      },
    ],
  ]);

  const result = StatusMap.get(action.type as StatusType);

  if (result === undefined) {
    throw new Error(`Unexpected value [${action.type}]`);
  }

  return result;
};
const StoreProvider = ({ children }: { children: any }): JSX.Element => {
  const initialState = {
    Lat: "",
    lng: "",
    coffeeStores: [],
  };
  const [state, dispatch] = useReducer(storeReducer, initialState);
  return (
    <storeContext.Provider value={{ state, dispatch }}>
      {children}
    </storeContext.Provider>
  );
};
export default StoreProvider;
