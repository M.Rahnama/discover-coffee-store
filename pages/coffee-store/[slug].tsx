import { useRouter } from "next/router";
import { ReactNode, useContext, useEffect, useState } from "react";
import CoffeeStoreCard from "../../components/CoffeeStoreCard";
import { getCoffeeStoreApi } from "../../lib/getCoffeeStoreApi";
import isEmpty from "../../helper/isEmpty";
import { storeContext } from "../../store/storeContext";
import axios from "axios";

interface ISpecifications {
  id: string;
  name: string;
  imgUrl: string;
  link: string;
  AddressInDetail: string;
}
interface ICoffeeStore {
  props: {
    coffeeStore: ISpecifications | undefined;
  };
}

interface IPaths {
  paths: {
    params: {
      slug: string;
    };
  }[];
  fallback?: boolean;
}

export async function getStaticProps({ params }: any): Promise<ICoffeeStore> {
  const coffeeStore = await getCoffeeStoreApi();
  const FindCoffeeStore = coffeeStore.find((item: ISpecifications): boolean => {
    return item.id === params.slug;
  });
  return {
    props: {
      coffeeStore: FindCoffeeStore ? FindCoffeeStore : {},
    },
  };
}

export async function getStaticPaths(): Promise<IPaths> {
  const coffeeStore = await getCoffeeStoreApi();

  const paths = coffeeStore.map(
    (item: ISpecifications): { params: { slug: string } } => {
      return {
        params: { slug: item.id },
      };
    }
  );
  return {
    paths,
    fallback: true,
  };
}

const CoffeStore = (initialProps: {
  coffeeStore: ISpecifications;
}): ReactNode => {
  const router = useRouter();
  const slug = router.query.slug;
  const [coffeeStore, setCoffeeStore] = useState(initialProps.coffeeStore);
  const {
    state: { coffeeStores },
  } = useContext(storeContext);

  const handleCreateCoffeeStore = async (coffeeStore: {
    id: any;
    name: any;
    AddressInDetail: any;
    imgUrl: any;
  }) => {
    try {
      const { id, name, AddressInDetail, imgUrl } = coffeeStore;
      const response = await axios("/api/createCoffeeStore", {
        method: "post",
        data: { id: `${id}`, name, AddressInDetail, imgUrl },
      });
      console.log("🚀 ~ file: [slug].tsx:81 ~ response", response.data);
    } catch (error) {
      console.log("🚀 ~ file: [slug].tsx:74 ~ error", error);
    }
  };
  useEffect((): void => {
    if (isEmpty(initialProps.coffeeStore)) {
      if (coffeeStores.length > 0) {
        const coffeeStoreFromContext = coffeeStores.find(
          (item: ISpecifications): boolean => {
            return item.id === slug;
          }
        );
        if (coffeeStoreFromContext) {
          handleCreateCoffeeStore(coffeeStoreFromContext);
          setCoffeeStore(coffeeStoreFromContext);
        }
      } else {
        handleCreateCoffeeStore(initialProps.coffeeStore);
      }
    }
  }, [coffeeStores, initialProps.coffeeStore, slug]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <CoffeeStoreCard data={coffeeStore} />
    </>
  );
};

export default CoffeStore;
