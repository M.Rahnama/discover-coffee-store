import Airtable, {
  Collaborator,
  Attachment,
  FieldSet,
  Records,
} from "airtable";

const base = new Airtable({
  apiKey: process.env.NEXT_PUBLIC_AIRTABLE_API_KEY,
}).base(process.env.NEXT_PUBLIC_AIRTABLE_BASE_KEY!);

export const table = base("coffee-store-table");

interface ICoffeeStoreRecords {
  [x: string]:
    | string
    | number
    | boolean
    | Collaborator
    | readonly Collaborator[]
    | readonly string[]
    | readonly Attachment[]
    | undefined;
}
[];

export const getSpecificRecords = (
  records: Records<FieldSet>
): ICoffeeStoreRecords[] =>
  records.map(
    (record: { fields: ICoffeeStoreRecords }): ICoffeeStoreRecords => {
      return {
        ...record.fields,
      };
    }
  );
