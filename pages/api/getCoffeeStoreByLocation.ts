import { NextApiRequest, NextApiResponse } from "next";
import { getCoffeeStoreApi } from "../../lib/getCoffeeStoreApi";

const getCoffeeStoreByLocation = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  try {
    const { Lat, lng } = req.query;

    console.log(
      "🚀 ~ file: getCoffeeStoreByLocation.ts:12 ~ typeof Lat",
      req.query
    );
    if (typeof Lat === "string" && typeof lng === "string") {
      const response = await getCoffeeStoreApi(Lat, lng);

      return res.status(200).json(response);
    }
  } catch (error) {
    res.status(500);
    console.log("🚀 ~ file: getCoffeeStoreByLocation.ts:17 ~ error", error);
  }
};

export default getCoffeeStoreByLocation;
