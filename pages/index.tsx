import Head from "next/head";
import Banner from "../components/Banner";
import HeroImage from "../components/HeroImage";
import styles from "../styles/Home.module.css";
import heroImage from "../public/images/heroImage.png";
import Card from "../components/Card";
import { ReactNode, useContext, useEffect } from "react";
import { getCoffeeStoreApi } from "../lib/getCoffeeStoreApi";
import useTrakingLocation from "../hook/useTrakingLocation";
import { storeContext } from "../store/storeContext";
import axios from "axios";

interface ISpecifications {
  id: number;
  name: string;
  imgUrl: string;
  shortAddress: string;
}
interface ICoffeeStoreServer {
  props: {
    coffeeStore: ISpecifications[];
  };
}

interface ICoffeeStoreClient {
  coffeeStore: ISpecifications[];
}

export async function getStaticProps(): Promise<
  ICoffeeStoreServer | undefined
> {
  try {
    const coffeeStore = await getCoffeeStoreApi();

    return {
      props: {
        coffeeStore,
      },
    };
  } catch (error) {
    console.log(
      "🚀 ~ file: [slug].tsx ~ line 48 ~ getStaticProps ~ error",
      error
    );
  }
}

export default function Home(props: ICoffeeStoreClient): ReactNode {
  const { coffeeStore } = props;

  const { handleTrackLocation, isFindingLocation } = useTrakingLocation();
  const { dispatch, state } = useContext(storeContext);
  const { coffeeStores, Lat, lng } = state;

  const handleLocationAction = () => {
    handleTrackLocation();
  };

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      if (Lat && lng) {
        const coffeeStores = await axios
          .get(`/api/getCoffeeStoreByLocation?Lat=${Lat}&lng=${lng}`)
          .then((item): any => item.data);
        dispatch({
          type: process.env.NEXT_PUBLIC_SET_COFFEE_STORES || "",
          payload: { coffeeStores },
        });
      }
    };

    fetchData().catch(console.error);
  }, [Lat, lng]);

  return (
    <div className={styles.container}>
      <Head>
        <title>کافه یاب</title>
        <meta name="description" content="سامانه جستجوی نزدیک ترین کافی شاپ" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.wrapperMainBox}>
          <Banner
            title="کافه یاب"
            description="نزدیک ترین کافی شاپ به خودت رو پیدا کن !"
            searchButtonTilte={
              isFindingLocation ? "در حال جستجو..." : "جستجو کن !"
            }
            handleLocation={handleLocationAction}
          />
          <HeroImage url={heroImage} alt="heroImage.png" />
        </div>

        {coffeeStores?.length !== 0 && (
          <>
            <div className={styles.wrapperTitle}>
              <p className={styles.title}>کافه های نزدیک به شما</p>
            </div>
            <div className={styles.wrapperCardBoxes}>
              {coffeeStores.map((item: any): JSX.Element => {
                return (
                  <Card
                    key={item.id}
                    href={`/coffee-store/${item.id}`}
                    title={item.name}
                    description={item.shortAddress}
                    imgUrl={item.imgUrl}
                    imgAlt={item.name}
                  />
                );
              })}
            </div>
          </>
        )}
        {coffeeStore.length && (
          <>
            <div className={styles.wrapperTitle}>
              <p className={styles.title}>کافه های کرج </p>
            </div>
            <div className={styles.wrapperCardBoxes}>
              {coffeeStore.map((item: any): JSX.Element => {
                return (
                  <Card
                    key={item.id}
                    href={`/coffee-store/${item.id}`}
                    title={item.name}
                    description={item.shortAddress}
                    imgUrl={item.imgUrl}
                    imgAlt={item.name}
                  />
                );
              })}
            </div>
          </>
        )}
      </main>
    </div>
  );
}
