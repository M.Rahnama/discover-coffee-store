import StoreProvider from "../store/storeContext";
import "../styles/globals.css";
import type { AppProps } from "next/app";

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <StoreProvider>
      <Component {...pageProps} />
    </StoreProvider>
  );
}
