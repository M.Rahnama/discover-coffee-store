import Image from "next/image";
import Link from "next/link";
import styles from "./Card.module.css";

type Props = {
  title: string;
  description: string;
  imgUrl: string;
  href: string;
  imgAlt: string;
};

const Card = (props: Props): JSX.Element => {
  const { href, title, imgUrl, imgAlt, description } = props;
  return (
    <Link href={href} className={styles.wrapperCard}>
      <Image
        className={styles.imageCard}
        src={imgUrl}
        alt={imgAlt}
        width={450}
        height={600}
        placeholder={"blur"}
        blurDataURL={
          "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
        }
      />
      <div className={styles.wrapperCardTexts}>
        <h2 className={styles.titleCard}>{title}</h2>
        <p className={styles.descriptionCard}>{description}</p>
      </div>
    </Link>
  );
};

export default Card;
