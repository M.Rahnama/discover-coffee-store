import Image from "next/image";
import styles from "./CoffeeImage.module.css";
type Props = {
  imageUrl: string;
  alt: string;
  width?: number;
  height?: number;
};

const CoffeeImage = (props: Props): JSX.Element => {
  const { imageUrl, alt, width, height } = props;
  return (
    <div className={styles.wrapperCoffeeImage}>
      <Image
        src={
          imageUrl ||
          "https://s6.uupload.ir/filelink/ssRbVwdQgStC_683766d1bb/photo-1504753793650-d4a2b783c15e_jxjx.webp"
        }
        alt={alt}
        width={width || 300}
        height={height || 500}
      />
    </div>
  );
};

export default CoffeeImage;
