const isEmpty = (key: {}): boolean | 0 => {
  return (
    key === undefined ||
    (Object.keys(key).length === 0 && key.constructor === Object)
  );
};
export default isEmpty;
